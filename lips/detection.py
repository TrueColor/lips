import cv2
import mediapipe as mp
import numpy as np
from scipy.ndimage import binary_dilation

from lips.utils import get_mask, bounding_box, build_slices, get_coordinates, get_circles

lipsUpperOuter = [61, 185, 40, 39, 37, 0, 267, 269, 270, 409, 291]
lipsLowerOuter = [61, 146, 91, 181, 84, 17, 314, 405, 321, 375, 291]
lipsUpperInner = [78, 191, 80, 81, 82, 13, 312, 311, 310, 415, 308]
lipsLowerInner = [78, 95, 88, 178, 87, 14, 317, 402, 318, 324, 308]
fingertips = [4, 8, 12, 16, 20]


def get_lip_contour(landmarks, points_no, h, w):
    return np.array([get_coordinates(landmarks[no], h, w) for no in points_no])


def make_shift(contour, shift):
    return contour - shift


def get_circle(landmarks, points_no, h, w, alpha=.75, beta=.167):
    coords = np.array([get_coordinates(landmarks[no], h, w) for no in points_no])

    dist = np.linalg.norm(np.diff(coords, axis=0))

    center = alpha * coords[0] + (1 - alpha) * coords[-1]
    radius = dist * beta

    return center, radius


def get_color_by_circle(image, center, radius):
    h, w, c = image.shape
    y, x = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((x - center[0]) ** 2 + (y - center[1]) ** 2)

    mask = dist_from_center <= radius

    color = tuple(map(lambda el: int(np.floor(el)), image[mask].mean(axis=0)))
    std = image[mask].std(axis=0)
    return color, std


def draw_circles(image, landmarks, points_no, radius=30, color=(0, 255, 0)):
    h, w, _ = image.shape
    for p in np.array([get_coordinates(landmarks[no], h, w) for no in points_no]):
        image = cv2.circle(image, tuple(p.astype(int).tolist()), radius, color, thickness=3)
    return image


def smooth_lips(image, contours, struct=np.ones((7, 7), dtype=bool), ksize=(5, 5)):
    '''
    inplace function!!!

    :param image:
    :param contours:
    :param struct:
    :param ksize:
    :return:
    '''
    mask = get_mask(image.shape[:2], contours)
    box = bounding_box(np.concatenate(contours))

    cropped_mask = mask[build_slices(*box)]

    dilated_mask = binary_dilation(cropped_mask, struct)

    cropped_image = image[build_slices(*box)]

    cropped_image = cv2.GaussianBlur(cropped_image, ksize, cv2.BORDER_DEFAULT)
    image[build_slices(*box)][dilated_mask] = cropped_image[dilated_mask]

    return image


def lipstick(image, alpha=1):
    mp_face_mesh = mp.solutions.face_mesh
    mp_hands = mp.solutions.hands

    with mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5) as face_mesh, \
            mp_hands.Hands(max_num_hands=1, min_detection_confidence=0.5, min_tracking_confidence=0.5) as hands:
        image_clear = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_ret = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        image_clear.flags.writeable = False
        face_res = face_mesh.process(image_clear)

        image_ret.flags.writeable = True
        image_ret = cv2.cvtColor(image_ret, cv2.COLOR_RGB2BGR)

        h, w, _ = image_ret.shape

        contours = []
        if face_res.multi_face_landmarks:
            lips_upper = lipsUpperOuter + lipsUpperInner[::-1]
            contours.append(get_lip_contour(face_res.multi_face_landmarks[0].landmark, lips_upper, h, w))

            lips_lower = lipsLowerOuter + lipsLowerInner[::-1]
            contours.append(get_lip_contour(face_res.multi_face_landmarks[0].landmark, lips_lower, h, w))

            hands_res = hands.process(image_clear)

            if hands_res.multi_hand_landmarks:
                image_clear = cv2.cvtColor(image_clear, cv2.COLOR_RGB2BGR)
                h, w, _ = image_ret.shape
                center, radius = get_circle(hands_res.multi_hand_landmarks[0].landmark, [8, 7], h, w)

                color = get_color_by_circle(image_clear, center, radius)

                overlay = image_ret.copy()

                cv2.fillPoly(overlay, pts=contours, color=color)

                cv2.addWeighted(overlay, alpha, image_ret, 1 - alpha, 0, image_ret)

                image_clear_lab = cv2.cvtColor(image_clear, cv2.COLOR_BGR2LAB).astype(int)
                image_lab = cv2.cvtColor(image_ret, cv2.COLOR_BGR2LAB)
                color_lab = cv2.cvtColor(np.uint8([[list(color)]]), cv2.COLOR_BGR2LAB)[0][0].astype(int)

                mask = np.sum(image_clear_lab, axis=-1) != np.sum(image_lab, axis=-1)
                means_lab = np.mean(image_clear_lab[mask], axis=0).astype(int)

                image_clear_lab[mask] += (color_lab - means_lab)[None]
                image_clear_lab = np.clip(image_clear_lab, 0, 255).astype(np.uint8)
                image_ret = cv2.cvtColor(image_clear_lab, cv2.COLOR_LAB2BGR)

                smooth_lips(image_ret, contours)

                image_ret = cv2.circle(image_ret, tuple(center.astype(int).tolist()), radius.astype(int), (0, 0, 255),
                                       thickness=2)

                skin_color = get_color_by_circle(image_clear,
                                                 *get_circle(hands_res.multi_hand_landmarks[0].landmark, [5, 6], h, w,
                                                             alpha=1))

                print(skin_color)

    return cv2.cvtColor(image_ret, cv2.COLOR_BGR2RGB)


def find_lips_contours(image):
    mp_face_mesh = mp.solutions.face_mesh
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    h, w, _ = image_rgb.shape

    with mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5) as face_mesh:
        face_res = face_mesh.process(image_rgb)

        contours = []
        if face_res.multi_face_landmarks:
            lips_upper = lipsUpperOuter + lipsUpperInner[::-1]
            contours.append(get_lip_contour(face_res.multi_face_landmarks[0].landmark, lips_upper, h, w))

            lips_lower = lipsLowerOuter + lipsLowerInner[::-1]
            contours.append(get_lip_contour(face_res.multi_face_landmarks[0].landmark, lips_lower, h, w))

        return contours


def paint_lips_sift(image, template_path, c_no):
    cs, r, perspective = get_circles(image, template_path=template_path)
    contours = find_lips_contours(image)
    image_ret = np.array(image)
    mask = np.zeros_like(image_ret[..., 0])
    mask = cv2.fillPoly(mask, contours, 1) == 1

    perspectiveHLS = cv2.cvtColor(perspective, cv2.COLOR_BGR2HLS)
    image_retHLS = cv2.cvtColor(image_ret, cv2.COLOR_BGR2HLS)

    mean = image_retHLS[mask].mean(axis=0).astype(int)
    std = image_retHLS[mask].std(axis=0)

    new_mean, new_std = np.array(get_color_by_circle(perspectiveHLS, cs[c_no], r / 2))

    image_retHLS[mask] = (image_retHLS[mask] - mean) * new_std / std + new_mean
    image_retHLS[mask, 0] = new_mean[0]
    image_retHLS = np.clip(image_retHLS, (0, 0, 0), (180, 255, 255)).astype(np.uint8)
    image_ret = cv2.cvtColor(image_retHLS, cv2.COLOR_HLS2BGR)

    smooth_lips(image_ret, contours, struct=np.ones((20, 20), dtype=bool), ksize=(15, 15))
    return image_ret
