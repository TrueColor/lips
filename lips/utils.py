import cv2
import numpy as np


def get_coordinates(landmark, h, w):
    x = int(np.floor(landmark.x * w))
    y = int(np.floor(landmark.y * h))
    return x, y


def bounding_box(points, alpha=1.2):
    center = points.mean(axis=0)
    y, x = zip(*((points - center) * alpha + center))

    return np.array([[min(x), min(y)], [max(x), max(y)]], dtype=int)


def build_slices(start, stop):
    """
    Returns a tuple of slices built from ``start`` and ``stop``.

    Examples
    --------
    >>> build_slices([1, 2, 3], [4, 5, 6])
    (slice(1, 4), slice(2, 5), slice(3, 6))
    >>> build_slices([10, 11])
    (slice(10), slice(11))
    """
    if stop is not None:
        return tuple(map(slice, start, stop))

    return tuple(map(slice, start))


def get_mask(shape, contours):
    mask = np.zeros(shape, np.uint8)

    return cv2.drawContours(mask, contours, -1, (1,), 1)


def is_square(points):
    if len(points) != 4:
        return False
    area = cv2.contourArea(points)
    perimeter = cv2.arcLength(points, True)
    ar = area * 16 / perimeter ** 2
    return 0.95 <= ar <= 1.05


def reorder_from_idx(idx, a):
    return a[idx:] + a[:idx]


def reorder(points):
    perm = [0, 1, 2, 3]
    first = np.linalg.norm(points, axis=-1).argmin()
    perm = reorder_from_idx(first, perm)
    reordered_vertices = points[perm]
    if reordered_vertices[1][0] < reordered_vertices[3][0]:
        tmp = perm[1]
        perm[1] = perm[3]
        perm[3] = tmp
    return points[perm]


def cvt2gray(image):
    if image.ndim == 3:
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    elif image.ndim == 2:
        return image
    else:
        raise ValueError(image.ndim)


def find_box(image, template_path, *,
             min_match_count=10, image_w=500, template_w=400):
    template = cv2.imread(template_path, 0)
    img = cvt2gray(image)

    if img.ndim == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    scale_t = template_w / template.shape[1]
    template = cv2.resize(template, (0, 0), fx=scale_t, fy=scale_t)

    scale_im = image_w / img.shape[1]
    img = cv2.resize(img, (0, 0), fx=scale_im, fy=scale_im)

    # Initiate SIFT detector
    sift = cv2.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(template, None)
    kp2, des2 = sift.detectAndCompute(img, None)

    index_params = dict(algorithm=1, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)
    # store all the good matches as per Lowe's ratio test.
    good = [m for m, n in matches if m.distance < 0.7 * n.distance]

    if len(good) > min_match_count:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        h, w = template.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M).squeeze()
        box = reorder(dst) / scale_im
        return box  # FIXME: check the shape of the `box`

    return None


def get_box_perspective(image, points, height=420, width=594):
    matrix = cv2.getPerspectiveTransform(np.float32(reorder(points)),
                                         np.float32([[0, 0], [width - 1, 0], [width - 1, height - 1], [0, height - 1]]))

    return cv2.warpPerspective(image, matrix, (width, height), cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT,
                               borderValue=(255, 255, 255))


CENTERS = np.array([[0.20763023, 0.39170984],
                    [0.20763023, 0.59585492],
                    [0.49889949, 0.18756477],
                    [0.50036684, 0.39378238],
                    [0.49889949, 0.59896373],
                    [0.49889949, 0.80725389],
                    [0.78356566, 0.39378238],
                    [0.78356566, 0.59896373]])

R = 0.11005135730007337


def get_circles(image, template_centers=CENTERS, template_centers_r=R, **kwargs):
    box = find_box(image, **kwargs)
    perspective = get_box_perspective(image, box)
    shape = perspective.shape[:2]
    centers = template_centers * np.array(shape)
    r = shape[0] * template_centers_r
    return centers[:, [1, 0]], r, perspective  # FIXME


