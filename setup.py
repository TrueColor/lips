from setuptools import setup, find_packages

from lips.__version__ import __version__

with open('requirements.txt', encoding='utf-8') as file:
    requirements = file.read().splitlines()

setup(
    name='lips',
    version=__version__,
    packages=find_packages(include=('lips',)),
    install_requires=requirements,
    python_requires='>=3.6',
)
