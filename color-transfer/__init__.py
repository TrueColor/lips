from PIL import Image
import sys
from lips import detection
import cv2


if __name__ == '__main__':
    filename = sys.argv[1]
    output_filename = sys.argv[2]
    image = cv2.imread(filename)

    new_image = detection.lipstick(image)
    im = Image.fromarray(new_image)
    im.save(output_filename)
    im.close()
